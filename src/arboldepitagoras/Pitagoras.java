package ArbolPitagoras;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
public class Pitagoras extends Frame {

    ArrayList<Puntos2D> v = new ArrayList();

    public static void main(String args[]) {new Pitagoras();}

    

    public Pitagoras() {
        setSize(900, 900);
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent evt) {
                int x1 = evt.getX();
                int y1 = evt.getY();
                Puntos2D p = new Puntos2D(x1, y1);
                v.add(p);
                repaint();
            }
        });
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        setVisible(true);
    }

    private void dibujarArbol(Graphics g, int x1, int y1, int x2, int y2, int limite) {
        if (limite == 13) {
            return;
        }
        int dx = x2 - x1;
        int dy = y1 - y2;
        int x3 = x2 - dy;
        int y3 = y2 - dx;
        int x4 = x1 - dy;
        int y4 = y1 - dx;
        int x5 = (int) (x4 + 0.5F * (dx - dy));
        int y5 = (int) (y4 - 0.5F * (dx + dy));
        g.setColor(Color.BLACK);
        int xC[] = {x1, x2, x3, x4};
        int yC[] = {y1, y2, y3, y4};
        g.fillPolygon(xC, yC, 4);
        g.setColor(Color.CYAN);
        int xT[] = {x3, x4, x5};
        int yT[] = {y3, y4, y5};
        g.fillPolygon(xT, yT, 3);
        dibujarArbol(g, x4, y4, x5, y5, limite + 1);
        dibujarArbol(g, x5, y5, x3, y3, limite + 1);
    }

    public void paint(Graphics g) {
        if (v.size() >= 2) {
            dibujarArbol((Graphics2D) g, v.get(0).x, v.get(0).y, v.get(1).x, v.get(1).y, 0);
            v.clear();

        }
    }
    
    public int color(){return (int) (Math.random() * 256);}

}
